import React, { Component } from 'react';
import { UIManager } from 'react-native';
import { ThemeProvider } from 'styled-components';
import ApolloProvider from 'react-apollo/ApolloProvider';
import { store, client } from './store/index';

import { colors } from './config/colors'

import AppNavigation from './navigation';
// import Homepage from './containers/LandingPage/index';


if (UIManager.setLayoutAnimationEnabledExperimental) {
  UIManager.setLayoutAnimationEnabledExperimental(true);
}

class App extends Component {
  render() {
    return (
      <ApolloProvider store={store} client={client}>
        <ThemeProvider theme={colors}>
          <AppNavigation />
        </ThemeProvider>
      </ApolloProvider>
    );
  }
}

export default App;